'use strict';
//import * as d3 from 'https://unpkg.com/d3?module'
import flare from "./flare.js";

export default function () {
    const data = flare;

    const color = d3.scaleLinear()
        .domain([0, 5])
        .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
        .interpolate(d3.interpolateHcl);

    const format = d3.format(",d");
    const height = 932;
    const width = 932;
    const pack = (data) => d3.pack()
        .size([width, height])
        .padding(3)
        (d3.hierarchy(data)
            .sum(d => d.size)
            .sort((a, b) => b.value - a.value))
    const chart = {
        
        const root = pack(data);
    const min = Math.min.apply(null, root.descendants().map(d => d.r));
    const max = Math.max.apply(null, root.descendants().map(d => d.r));
    const scale = (r) => Math.sqrt((r - min) / (max - min)) * 2000;
    console.log(color(1));

    const svg = d3.select(DOM.svg(width, height))
        .style("font", "10px sans-serif")
        .style("width", "100%")
        .style("height", "auto")
        .attr("text-anchor", "middle");

    const node = svg.selectAll("g")
        .data(root.descendants())
        .enter().append("g")
        .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

    node.append("circle")
        .on("mouseover", function () {
            d3.select(this)
                .transition("chart-stroke-update")
                .duration(200)
                .attr("stroke-width", 3)
        })
        .on("mouseout", function () {
            d3.select(this)
                .transition("chart-stroke-update")
                .duration(200)
                .attr("stroke-width", 1)
        })
        .transition("ease")
        .delay((d, i) => 2000 - scale(d.r))
        .duration(d => 500)
        .attr("r", d => d.r)
        .attr("fill", d => color(d.height))
        .attr("stroke", d => color2(d.height));


    const leaf = node.filter(d => !d.children);

    leaf.select("circle")
        .attr("id", d => (d.leafUid = DOM.uid("leaf")).id);
    //.attr("stroke", "#000");

    leaf.append("clipPath")
        .attr("id", d => (d.clipUid = DOM.uid("clip")).id)
        .append("use")
        .attr("xlink:href", d => d.leafUid.href);

    leaf.append("text")
        .attr("clip-path", d => d.clipUid)
        .style("pointer-events", "none")
        .selectAll("tspan")
        .data(d => d.data.name.split(/(?=[A-Z][^A-Z])/g))
        .enter().append("tspan")
        .attr("x", 0)
        .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
        .text(d => d);

    node.append("title")
        .text(d => `${d.ancestors().map(d => d.data.name).reverse().join("/")}\n${format(d.value)}`);

    return svg.node();
    }
}