//import shakespeare from "./shakespeare.js";
//import flare from "./flare.js";

class Poetry {

    constructor(shakespeare) {
        this.shakespeare = shakespeare;
        console.log(this.shakespeare);
        this.shakespeare();
    }

    }
    class DOMClass {
        constructor() {this.counter = 0}
        uid(type) {
            return 'ffi_' + type + '_' + this.counter++;
        }
    }

    const DOM = new DOMClass();
    //const data = flare;

    const color = d3.scaleLinear()
        .domain([0, 5])
        .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
        .interpolate(d3.interpolateHcl);
    const color2 = function() { return '#EEE'; };
    const format = d3.format(",d");
    const height = 2880;
    const width = 2880;
    const pack = (data) => d3.pack()
        .size([width, height])
        .padding(3)
        (d3.hierarchy(data)
            .sum(d => d.size)
            .sort((a, b) => b.value - a.value))


   function chart(data){
        
      
        const root = pack(data);
    const min = Math.min.apply(null, root.descendants().map(d => d.r));
    const max = Math.max.apply(null, root.descendants().map(d => d.r));
    const scale = (r) => Math.floor(Math.sqrt((r - min) / (max - min)) * 2000);
    // console.log(color(1));
    
    const svg = d3.select('svg');
    //svg[0]
    svg.style("font", "12px sans-serif")
        .style("width", width)
        .style("height", height)
        .attr("text-anchor", "middle");

    const node = svg.selectAll("g")
        .data(root.descendants())
        .enter().append("g")
        .attr("transform", d => `translate(${Math.floor(d.x + 1)},${Math.floor(d.y + 1)})`);

    node.append("circle")
        .on("mouseover", function () {
            d3.select(this)
                .transition("chart-stroke-update")
                .duration(200)
                .attr("stroke-width", 3)
        })
        .on("mouseout", function () {
            d3.select(this)
                .transition("chart-stroke-update")
                .duration(200)
                .attr("stroke-width", 1)
        })
        .transition("ease")
        .delay((d, i) => 2000 - scale(d.r))
        .duration(d => 500)
        .attr("r", d => Math.floor(d.r))
        .attr("fill", d => color(d.height))
        .attr("stroke", d => color2(d.height));


    const leaf = node.filter(d => !d.children);

    leaf.select("circle")
        .attr("id", d => (d.leafUid = DOM.uid("leaf")).id);
    //.attr("stroke", "#000");

    leaf.append("clipPath")
        .attr("id", d => (d.clipUid = DOM.uid("clip")).id)
        .append("use")
        .attr("xlink:href", d => d.leafUid.href);

    leaf.append("text")
        .attr("clip-path", d => d.clipUid)
        .style("pointer-events", "none")
        .selectAll("tspan")
        .data(d => 
            d.data.name.split(/(?=[A-Z][^A-Z])/g))
        .enter().append("tspan")
        .attr("x", 0)
        .attr("y", (d, i, nodes) => `${i - nodes.length / 2 + 0.8}em`)
        .text(d => d);

    node.append("title")
        .text(d => `${d.ancestors().map(d => d.data.name).reverse().join("/")}\n${format(d.value)}`);

    return svg.node();
    }

    function massage(raw_data) {
        const result = {};
        const children =  [];
        result.name = "Shakespeare Sonnet";
        result.children = raw_data;
        /*for(let i = 0; i < raw_data.length; i++) {
            let d = raw_data[i];  
            const name = Object.keys(d)[0];
            if ( !['and', 'the', 'to', 'of', 's', 'a'].includes(name)) {
                const size = d[name];
                children.push({'name':name, 'size': Math.log2(Number(size))});
            }
            
        }*/
        console.log(result);
        return result;
    }
   d3.json("/shakespeare_sonnets_elaborated.json").then(data => chart(data));
d3.select("#save").on("click", function(){
    var html = d3.select("svg")
        .attr("version", 1.1)
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .node().parentNode.innerHTML;

    console.log(html);
    var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
    var img = '<img src="'+imgsrc+'">';
    d3.select("#svgdataurl").html(img);
    

  var canvas = document.querySelector("canvas"),
	  context = canvas.getContext("2d");

  var image = new Image;
  image.src = imgsrc;
  image.onload = function() {
	  context.drawImage(image, 0, 0);

	  var canvasdata = canvas.toDataURL("image/png");

	  var pngimg = '<img src="'+canvasdata+'">'; 
  	  d3.select("#pngdataurl").html(pngimg);

	  var a = document.createElement("a");
	  a.download = "sample.png";
	  a.href = canvasdata;
          document.body.appendChild(a);
	  a.click();
  };
});

// new Poetry(shakespeare);