const express = require('express');
const path = require('path');
const app = express();
const port = 3000;
const myLogger = function (req, res, next) {
    console.log('LOGGED ' + req.url);
    //console.log("\t" + header + " " + req.get(header));
    let properties = Object.keys(req);
   // console.log(properties);
    let headers = properties['headers'];
    for( pp in headers) {
        console.log( "\t" + headers[pp]);
    }
    next();
};
const d3lib = 'index.js';
app.use(myLogger);
app.get('/',
    (req, res) => {
        console.log("ciao");
        res.sendFile(path.join(__dirname,'public/index.html'))
    });
// app.get('/', (req, res) => res.send('Hello World!'));
// app.use('/static/d3.js',
//     express.static( path.join(__dirname, '../node_modules/d3/' + d3lib)));
app.use('/static/',
    express.static( path.join(__dirname, '../node_modules/')));

app.use('/',
    express.static( path.join(__dirname, 'public')));
app.use('/js',

        express.static( path.join(__dirname, 'js'))
    );

app.listen(port,
    (req) => console.log(`Example app listening on port ${port}! ${req}`));