'use strict';
// import WordPOS from ;
// ./node_modules/wordpos/src/
let fs = require("fs");

const WordPOS = require("wordpos");
const wnPos = new WordPOS();

let content = fs.readFileSync("src/public/shakespeare_sonnets_words.json");
let sonnetWords = JSON.parse(content);


//console.log(sonnetWords);


function getOrCreateCategories(categories, results, categoryName) {

    if (!(categoryName in categories)) {
        let tokens = categoryName.split('.');
        categories[categoryName] = {name: tokens[1], children: []};
        if (!(tokens[0] in categories)) {
            categories[tokens[0]] = {name: tokens[0], children: [categories[categoryName]]};
            results.push(categories[tokens[0]]);
        } else {
            categories[tokens[0]].children.push(categories[categoryName]);
        }
    }
    return categories[categoryName];
}

function insertWordWithType(categories, results, type, word, size) {
    // size > 1 ? Math.log2(Number(size)) + 1 : 0.1
    let row = {"name": word, "type": type, "size": size};
    let category = getOrCreateCategories(categories, results, type);
    category.children.push(row);
}

function extractType(categories, word, size, results) {
    return wnPos.lookup(word,
        data => {
            let type;
            if (data !== undefined && data.length > 0) {
                type = data[0].lexName;
            } else {
                type = "unknown";
            }
            insertWordWithType(categories, results, type, word, size );
        }
    );
}
const PRONOUNS_PERSONAL = ['i', 'me', 'we','us', 'you', 'you', 'thou', 'thee', 'thy', 'she', 'her','he','him', 'it', 'they','them'];
const PRONOUNS_RELATIVE = ['that', 'which', 'who', 'whom', 'whose', 'whichever', 'whoever', 'whomever'];
const PRONOUNS_DEMONSTRATIVE = ['this', 'these', 'that', 'those'];
const PRONOUNS_INDEFINITE = ['anybody', 'anyone', 'anything', 'each', 'either', 'everybody', 'everyone', 'everything',
'neither', 'nobody', 'none', 'nothing', /* one, */ 'somebody', 'someone', 'something',
'both', 'few', 'many', 'several', 'all', 'any', 'most', 'some'];
const PRONOUNS_REFLEXIVE = ['myself', 'ourselves', 'yourself', 'thyself', 'yourselves', 'himself', 'herself', 'itself', 'themselves'];
const PRONOUNS_INTERROGATIVE = ['what', 'who', 'which', 'whom', 'whose'];
const PRONOUNS_POSSESSIVE = ['my', 'your', 'his', 'her', 'its','our', 'your', 'their', 'thine', 'mine', 'yours', 'hers', 'ours', 'yours', 'theirs'];
const PREPOSITIONS = ['aboard', 'about', 'above', 'across', 'after', 'at',
    'around', 'before', 'behind', 'below', 'beside', 'between', 'by', 'down',
    'during', 'for', 'despite', 'circa', 'like', 'near', 'except', 'before',
    'from', 'in', 'inside', 'onto', 'of','off','on','out', 'past','plus',
    'regarding', 'till', 'through', 'to', 'towars', 'towards',
    'under', 'up', 'unlike', 'until', 'with', 'within','without'];
const CONJUNCTIONS = ['for', 'and', 'nor', 'but', 'or', 'yet', 'so'];
const SUBORDINATING = ['after','although', 'as', 'because', 'before',
    'by the time', 'if', 'lest', 'once', 'since', 'than', 'that', 'unless', 'until', 'when','whenever', 'where', 'wherever', 'while'];
const SIMPLIFICATION = {
    'is': 'be', 'were':'be', 'was': 'be', 'be' : 'be',
    'seen': 'see', 'saw' : 'see' , 'see' : 'see',
    'makes': 'make', 'made' : 'make', 'make' : 'make',
    'roses': 'rose', 'rose' : 'rose',
    'hast' : 'have', 'had': 'have', 'hath' : 'have', 'having' : 'have', 'have' : 'have',
    'took' : 'take', 'take': 'take',
    'doth': 'do', 'did':'do', 'do': 'do',
    'cannot': 'can', 'could':'can', 'can': 'can',
    'should' : 'shall', 'shalt' : 'shalt', 'shall':'shall'
};
const foundSimplification = {};
function buildResponse(index, sonnetWords, results, categories, callback) {
    if (index < sonnetWords.length) {
        let row = sonnetWords[index];
        let word = Object.keys(row)[0];
        const size = row[word];
        let type = null;
        if (SIMPLIFICATION[word]) {
            let sub = SIMPLIFICATION[word];
            foundSimplification[sub] = size + (foundSimplification[sub] > 0 ? foundSimplification[sub] : 0);
            buildResponse(index + 1, sonnetWords, results, categories, callback);
            return;
        }

        if (CONJUNCTIONS.includes(word)) {
            type = 'conjunction.coordinating';
        } else if (SUBORDINATING.includes(word)) {
            type = 'conjunction.subordinating';
        } else if (PRONOUNS_PERSONAL.includes(word)) {
            type = 'pronouns.personal';
        } else if (PRONOUNS_RELATIVE.includes(word)) {
            type = 'pronouns.relative';
        } else if (PRONOUNS_DEMONSTRATIVE.includes(word)) {
            type = 'pronouns.demonstrative';
        } else if (PRONOUNS_INDEFINITE.includes(word)) {
            type = 'pronouns.indefinite';
        } else if (PRONOUNS_REFLEXIVE.includes(word)) {
            type = 'pronouns.reflexive';
        } else if (PRONOUNS_INTERROGATIVE.includes(word)) {
            type = 'pronouns.interrogative';
        } else if (PRONOUNS_POSSESSIVE.includes(word)) {
            type = 'pronouns.possessive';
        } else if (PREPOSITIONS.includes(word)) {
            type = 'prepositions.p';
        }

        if (type !== null) {
            insertWordWithType(categories, results, type, word, size);
            buildResponse(index + 1, sonnetWords, results, categories, callback);
        } else {
            if (!['and', 'the', 'to', 'of', 's', 'a', '[', ']', '--', 'd'].includes(word)) {
                extractType(categories, word, size, results).then(() => buildResponse(index + 1, sonnetWords, results, categories, callback));
            } else {
                buildResponse(index + 1, sonnetWords, results, categories, callback);
            }
        }
    } else if ( Object.keys(foundSimplification).length >0 ) {
        let word = Object.keys(foundSimplification)[0];
        let size = foundSimplification[word];
        delete foundSimplification[word];
        console.log(word, size);
        extractType(categories, word, size, results).then( () => buildResponse(index, sonnetWords, results, categories, callback));
    } else {
        callback(results);
    }
}

let children = [{name: "unknown", children: []}];
let result = {name: "SS", children: children};
let categories = {unknown: children[0]};
buildResponse(0, sonnetWords, children, categories, (children) => {
    console.log("Processed: " + children.length);
    let asJSON = JSON.stringify(result, null, " ");
    //result.children.pop();
    fs.writeFileSync("src/public/shakespeare_sonnets_elaborated.json", asJSON);
});
