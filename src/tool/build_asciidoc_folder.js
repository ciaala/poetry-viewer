const fs = require('fs');
const  Asciidoctor = require("asciidoctor.js");
let asciidoctor = Asciidoctor();

let html = asciidoctor.convert(fs.readFileSync('docs/links.adoc'));

fs.mkdirSync('build/docs',{recursive:true});
fs.writeFileSync('build/docs/links.html', html);